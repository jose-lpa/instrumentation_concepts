FROM python:3.8.5-buster

LABEL maintainer="José L. Patiño-Andrés <jose.lpa@gmail.com>"


ENV TERM linux
ENV TEMRINFO /etc/terminfo

RUN mkdir -p /instrumentation
WORKDIR /instrumentation

COPY ./requirements.txt /instrumentation
RUN pip install -r requirements.txt

COPY ./src/ /instrumentation

ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

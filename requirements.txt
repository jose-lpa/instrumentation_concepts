docker-compose==1.26.2
fastapi==0.60.1
prometheus-client==0.8.0
uvicorn==0.11.8

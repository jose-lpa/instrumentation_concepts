# FastAPI integration with Prometheus

A small proof of concept for integrating Prometheus into a FastAPI-based Python 
application.

## Usage

Run it with Docker:

    docker-compose up --build

Then issue a couple of requests to the app to actually create some metrics. We
have 2 endpoints available:

    curl -s localhost:8000/
    curl -s localhost:8000/error/

It's advisable to automate a bunch of requests in order to have some hundreds
of them to see in Prometheus:

    for n in `seq 400`; do curl -s localhost:8000/; done
    for n in `seq 173`; do curl -s localhost:8000/error/; done

Now we can go to Prometheus dashboard and check up the collected metrics in
http://localhost:9090/graph

import time
from typing import Callable

from fastapi import Request, Response
from starlette.middleware.base import BaseHTTPMiddleware

from prometheus_client import Counter, Gauge, Histogram


class MetricsMiddleware(BaseHTTPMiddleware):
    """
    Middleware class to process metrics in every request.
    https://www.starlette.io/middleware/#basehttpmiddleware
    """
    request_latency = Histogram(
        "request_latency", "Request latency (s)", ["app_name", "endpoint"]
    )
    request_count = Counter(
        'request_count', 'Requests count',
        ['app_name', 'method', 'endpoint', 'http_status']
    )

    async def dispatch(
        self, request: Request, call_next: Callable[[Request], Response]
    ):
        start_time = time.time()
        response = await call_next(request)

        process_time = time.time() - start_time
        response.headers["X-Process-Time"] = str(process_time)

        self.request_latency.labels(
            "instrumentation", request.scope["path"]
        ).observe(process_time)
        self.request_count.labels(
            "instrumentation",
            request.scope["method"],
            request.scope["path"],
            response.status_code
        ).inc()

        return response

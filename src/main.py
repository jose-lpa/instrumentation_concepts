from fastapi import FastAPI, status
from fastapi.responses import JSONResponse, Response

from prometheus_client import generate_latest

from middleware import MetricsMiddleware


app = FastAPI()

app.add_middleware(MetricsMiddleware)


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/error/")
async def handled_error():
    return JSONResponse(
        {"message": "Holy Sh1t!"},
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
    )


@app.get("/metrics/")
async def metrics():
    CONTENT_TYPE_LATEST = "text/plain; version=0.0.4; charset=utf-8"

    return Response(generate_latest(), media_type=CONTENT_TYPE_LATEST)
